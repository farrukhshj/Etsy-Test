package com.test.etsy.etsysearch.network;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.test.etsy.etsysearch.controllers.MainActivity;
import com.test.etsy.etsysearch.model.ListingImageEntity.ListingImageEntity;
import com.test.etsy.etsysearch.model.ListingsEntity.ListingsEntity;
import com.test.etsy.etsysearch.model.ListingsEntity.Result;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DownloadService extends IntentService {

    private int responseCount = 0;
    private String TAG = "Download Service";
    private HashMap<Long, String> listingImageUrls;
    String API_KEY = "kiummtm58eij7garasx8ukjx";
    private String listingJSON;
    //String API_KEY = "1q0cqzxypbcdddm6ztoudz58";  Developer Key

    public DownloadService() {

        super("Download Service");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        int currentOffset = 0;
        int currentLimit = 0;
        String keyword = "";
        if (intent != null) {
            currentOffset = intent.getIntExtra("OFFSET", 20);
            currentLimit = intent.getIntExtra("LIMIT", 20);
            keyword = intent.getStringExtra("KEYWORD");
        }
        listingImageUrls = new HashMap<Long, String>();
        fetchFromEndpoint(currentOffset, currentLimit, keyword);

    }

    /**
     * This function uses the retrofit library to fetch the listings data from the web endpoint.
     * Also contains logic for Interceptor for logging http requests
     *
     * @param currentOffset
     * @param keyword
     * @param currentLimit
     */
    private void fetchFromEndpoint(int currentOffset, int currentLimit, String keyword) {

        EventBus.getDefault().register(this);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://openapi.etsy.com/v2/")
                .client(httpClient.build())
                .build();

        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);


        Call<ResponseBody> request = retrofitInterface.getAllListings(currentLimit, keyword, currentOffset, API_KEY);
        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    try {
                        String responseString = response.body().string();
                        onfetchComplete(listingJSON);

                        /*ListingsEntity temp = new Gson().fromJson(responseString, ListingsEntity.class);
                        List<Result> result = temp.getResults();
                        for (Result x : result) {
                           fetchImagesFromEndpoint(x.getListingId());
                        }
                        Log.d("frk", responseString);
                        saveListingJSON(responseString);*/

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }

        });
    }


    private boolean fetchImagesFromEndpoint(final Long listingsId) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://openapi.etsy.com/v2/")
                .client(httpClient.build())
                .build();

        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> request = retrofitInterface.getImagesForListing(listingsId, API_KEY);
        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        putImageURLinMap(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }

        });
        return true;
    }

    private void putImageURLinMap(String resultString) {
        ListingImageEntity temp = new Gson().fromJson(resultString, ListingImageEntity.class);
        if (temp.getResults().size() != 0) {
            listingImageUrls.put(temp.getResults().get(0).getListingId(), temp.getResults().get(0).getUrl75x75());
        }
    }


    /**
     * This function is called when we complete receiving data from web endpoint. Contains post GET logic like sending notification or
     * sending a broadcast to let activity know download completed.
     **/
    private void onfetchComplete(String listingsJson) {
        Intent intent = new Intent(MainActivity.DOWNLOAD_PROGRESS);
        intent.putExtra("LISTINGS_JSON", listingsJson);
        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

}
