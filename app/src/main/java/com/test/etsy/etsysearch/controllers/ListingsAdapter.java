package com.test.etsy.etsysearch.controllers;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.test.etsy.etsysearch.R;
import com.test.etsy.etsysearch.model.ListingImageEntity.ListingImageEntity;
import com.test.etsy.etsysearch.model.ListingsEntity.ListingsEntity;
import com.test.etsy.etsysearch.network.RetrofitInterface;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ListingsAdapter extends RecyclerView.Adapter<ListingsAdapter.ViewHolder> {
    private ListingsEntity listingsData;
    private HashMap<Long,String> imageURL;


    public ListingsAdapter(ListingsEntity mListingEntity, HashMap<Long, String> mapOfImageUrls) {
        this.listingsData = mListingEntity;
        this.imageURL = mapOfImageUrls;
    }

    @NonNull
    @Override
    public ListingsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listing_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onBindViewHolder(@NonNull final ListingsAdapter.ViewHolder holder, final int position) {
        holder.tvListingTitle.setText(listingsData.getResults().get(position).getTitle());
        holder.tvItemNumber.setText(String.valueOf(position));

        new AsyncTask<Long, Integer, String>() {
            @Override
            protected String doInBackground(Long... params) {

                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                httpClient.addInterceptor(logging);

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://openapi.etsy.com/v2/")
                        .client(httpClient.build())
                        .build();

                RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
                Call<ResponseBody> request = retrofitInterface.getImagesForListing(params[0], "kiummtm58eij7garasx8ukjx");
                request.enqueue(new Callback<ResponseBody>() {
                    public String resultString;

                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                resultString = response.body().string();
                                imageURL(resultString);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        t.printStackTrace();
                    }

                });

                return null;
            }

            public void imageURL(String result) {
                Gson gson = new Gson();
                ListingImageEntity temp = gson.fromJson(result, ListingImageEntity.class);
                Picasso.get().load(temp.getResults().get(0).getUrl75x75()).into(holder.ivListingImage);
            }

            @Override
            protected void onPostExecute(String result) {

            }
        }.execute(listingsData.getResults().get(position).getListingId());


    }

    @Override
    public int getItemCount() {
        return listingsData.getResults().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvListingTitle;
        TextView tvItemNumber;
        ImageView ivListingImage;

        public ViewHolder(View itemView) {
            super(itemView);
            tvListingTitle = itemView.findViewById(R.id.tvListingTitle);
            ivListingImage = itemView.findViewById(R.id.ivListingImage);
            tvItemNumber = itemView.findViewById(R.id.tvItemNumber);
        }
    }

    public void clearAdapter() {
        final int size = listingsData.getResults().size();
        listingsData.getResults().clear();
        notifyItemRangeRemoved(0, size);
    }

}
