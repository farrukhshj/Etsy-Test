
package com.test.etsy.etsysearch.model.ListingImageEntity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("listing_image_id")
    @Expose
    private Long listingImageId;
    @SerializedName("hex_code")
    @Expose
    private String hexCode;
    @SerializedName("red")
    @Expose
    private Long red;
    @SerializedName("green")
    @Expose
    private Long green;
    @SerializedName("blue")
    @Expose
    private Long blue;
    @SerializedName("hue")
    @Expose
    private Long hue;
    @SerializedName("saturation")
    @Expose
    private Long saturation;
    @SerializedName("brightness")
    @Expose
    private Long brightness;
    @SerializedName("is_black_and_white")
    @Expose
    private Boolean isBlackAndWhite;
    @SerializedName("creation_tsz")
    @Expose
    private Long creationTsz;
    @SerializedName("listing_id")
    @Expose
    private Long listingId;
    @SerializedName("rank")
    @Expose
    private Long rank;
    @SerializedName("url_75x75")
    @Expose
    private String url75x75;
    @SerializedName("url_170x135")
    @Expose
    private String url170x135;
    @SerializedName("url_570xN")
    @Expose
    private String url570xN;
    @SerializedName("url_fullxfull")
    @Expose
    private String urlFullxfull;
    @SerializedName("full_height")
    @Expose
    private Long fullHeight;
    @SerializedName("full_width")
    @Expose
    private Long fullWidth;

    public Long getListingImageId() {
        return listingImageId;
    }

    public void setListingImageId(Long listingImageId) {
        this.listingImageId = listingImageId;
    }

    public String getHexCode() {
        return hexCode;
    }

    public void setHexCode(String hexCode) {
        this.hexCode = hexCode;
    }

    public Long getRed() {
        return red;
    }

    public void setRed(Long red) {
        this.red = red;
    }

    public Long getGreen() {
        return green;
    }

    public void setGreen(Long green) {
        this.green = green;
    }

    public Long getBlue() {
        return blue;
    }

    public void setBlue(Long blue) {
        this.blue = blue;
    }

    public Long getHue() {
        return hue;
    }

    public void setHue(Long hue) {
        this.hue = hue;
    }

    public Long getSaturation() {
        return saturation;
    }

    public void setSaturation(Long saturation) {
        this.saturation = saturation;
    }

    public Long getBrightness() {
        return brightness;
    }

    public void setBrightness(Long brightness) {
        this.brightness = brightness;
    }

    public Boolean getIsBlackAndWhite() {
        return isBlackAndWhite;
    }

    public void setIsBlackAndWhite(Boolean isBlackAndWhite) {
        this.isBlackAndWhite = isBlackAndWhite;
    }

    public Long getCreationTsz() {
        return creationTsz;
    }

    public void setCreationTsz(Long creationTsz) {
        this.creationTsz = creationTsz;
    }

    public Long getListingId() {
        return listingId;
    }

    public void setListingId(Long listingId) {
        this.listingId = listingId;
    }

    public Long getRank() {
        return rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    public String getUrl75x75() {
        return url75x75;
    }

    public void setUrl75x75(String url75x75) {
        this.url75x75 = url75x75;
    }

    public String getUrl170x135() {
        return url170x135;
    }

    public void setUrl170x135(String url170x135) {
        this.url170x135 = url170x135;
    }

    public String getUrl570xN() {
        return url570xN;
    }

    public void setUrl570xN(String url570xN) {
        this.url570xN = url570xN;
    }

    public String getUrlFullxfull() {
        return urlFullxfull;
    }

    public void setUrlFullxfull(String urlFullxfull) {
        this.urlFullxfull = urlFullxfull;
    }

    public Long getFullHeight() {
        return fullHeight;
    }

    public void setFullHeight(Long fullHeight) {
        this.fullHeight = fullHeight;
    }

    public Long getFullWidth() {
        return fullWidth;
    }

    public void setFullWidth(Long fullWidth) {
        this.fullWidth = fullWidth;
    }

}
