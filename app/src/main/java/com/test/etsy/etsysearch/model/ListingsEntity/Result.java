
package com.test.etsy.etsysearch.model.ListingsEntity;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("listing_id")
 
    private Long listingId;
    @SerializedName("state")
 
    private String state;
    @SerializedName("user_id")
 
    private Long userId;
    @SerializedName("category_id")
 
    private Long categoryId;
    @SerializedName("title")
 
    private String title;
    @SerializedName("description")
 
    private String description;
    @SerializedName("creation_tsz")
 
    private Long creationTsz;
    @SerializedName("ending_tsz")
 
    private Long endingTsz;
    @SerializedName("original_creation_tsz")
 
    private Long originalCreationTsz;
    @SerializedName("last_modified_tsz")
 
    private Long lastModifiedTsz;
    @SerializedName("price")
 
    private String price;
    @SerializedName("currency_code")
 
    private String currencyCode;
    @SerializedName("quantity")
 
    private Long quantity;
    @SerializedName("sku")
 
    private List<Object> sku = null;
    @SerializedName("tags")
 
    private List<String> tags = null;
    @SerializedName("category_path")
 
    private List<String> categoryPath = null;
    @SerializedName("category_path_ids")
 
    private List<Long> categoryPathIds = null;
    @SerializedName("materials")
 
    private List<String> materials = null;
    @SerializedName("shop_section_id")
 
    private Long shopSectionId;
    @SerializedName("featured_rank")
 
    private Object featuredRank;
    @SerializedName("state_tsz")
 
    private Long stateTsz;
    @SerializedName("url")
 
    private String url;
    @SerializedName("views")
 
    private Long views;
    @SerializedName("num_favorers")
 
    private Long numFavorers;
    @SerializedName("shipping_template_id")
 
    private Long shippingTemplateId;
    @SerializedName("processing_min")
 
    private Long processingMin;
    @SerializedName("processing_max")
 
    private Long processingMax;
    @SerializedName("who_made")
 
    private String whoMade;
    @SerializedName("is_supply")
 
    private String isSupply;
    @SerializedName("when_made")
 
    private String whenMade;
    @SerializedName("item_weight")
 
    private Object itemWeight;
    @SerializedName("item_weight_units")
 
    private Object itemWeightUnits;
    @SerializedName("item_length")
 
    private Object itemLength;
    @SerializedName("item_width")
 
    private Object itemWidth;
    @SerializedName("item_height")
 
    private Object itemHeight;
    @SerializedName("item_dimensions_unit")
 
    private String itemDimensionsUnit;
    @SerializedName("is_private")
 
    private Boolean isPrivate;
    @SerializedName("recipient")
 
    private Object recipient;
    @SerializedName("occasion")
 
    private Object occasion;
    @SerializedName("style")
 
    private Object style;
    @SerializedName("non_taxable")
 
    private Boolean nonTaxable;
    @SerializedName("is_customizable")
 
    private Boolean isCustomizable;
    @SerializedName("is_digital")
 
    private Boolean isDigital;
    @SerializedName("file_data")
 
    private String fileData;
    @SerializedName("should_auto_renew")
 
    private Boolean shouldAutoRenew;
    @SerializedName("language")
 
    private String language;
    @SerializedName("has_variations")
 
    private Boolean hasVariations;
    @SerializedName("taxonomy_id")
 
    private Long taxonomyId;
    @SerializedName("taxonomy_path")
 
    private List<String> taxonomyPath = null;
    @SerializedName("used_manufacturer")
 
    private Boolean usedManufacturer;

    public Long getListingId() {
        return listingId;
    }

    public void setListingId(Long listingId) {
        this.listingId = listingId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCreationTsz() {
        return creationTsz;
    }

    public void setCreationTsz(Long creationTsz) {
        this.creationTsz = creationTsz;
    }

    public Long getEndingTsz() {
        return endingTsz;
    }

    public void setEndingTsz(Long endingTsz) {
        this.endingTsz = endingTsz;
    }

    public Long getOriginalCreationTsz() {
        return originalCreationTsz;
    }

    public void setOriginalCreationTsz(Long originalCreationTsz) {
        this.originalCreationTsz = originalCreationTsz;
    }

    public Long getLastModifiedTsz() {
        return lastModifiedTsz;
    }

    public void setLastModifiedTsz(Long lastModifiedTsz) {
        this.lastModifiedTsz = lastModifiedTsz;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public List<Object> getSku() {
        return sku;
    }

    public void setSku(List<Object> sku) {
        this.sku = sku;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(List<String> categoryPath) {
        this.categoryPath = categoryPath;
    }

    public List<Long> getCategoryPathIds() {
        return categoryPathIds;
    }

    public void setCategoryPathIds(List<Long> categoryPathIds) {
        this.categoryPathIds = categoryPathIds;
    }

    public List<String> getMaterials() {
        return materials;
    }

    public void setMaterials(List<String> materials) {
        this.materials = materials;
    }

    public Long getShopSectionId() {
        return shopSectionId;
    }

    public void setShopSectionId(Long shopSectionId) {
        this.shopSectionId = shopSectionId;
    }

    public Object getFeaturedRank() {
        return featuredRank;
    }

    public void setFeaturedRank(Object featuredRank) {
        this.featuredRank = featuredRank;
    }

    public Long getStateTsz() {
        return stateTsz;
    }

    public void setStateTsz(Long stateTsz) {
        this.stateTsz = stateTsz;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public Long getNumFavorers() {
        return numFavorers;
    }

    public void setNumFavorers(Long numFavorers) {
        this.numFavorers = numFavorers;
    }

    public Long getShippingTemplateId() {
        return shippingTemplateId;
    }

    public void setShippingTemplateId(Long shippingTemplateId) {
        this.shippingTemplateId = shippingTemplateId;
    }

    public Long getProcessingMin() {
        return processingMin;
    }

    public void setProcessingMin(Long processingMin) {
        this.processingMin = processingMin;
    }

    public Long getProcessingMax() {
        return processingMax;
    }

    public void setProcessingMax(Long processingMax) {
        this.processingMax = processingMax;
    }

    public String getWhoMade() {
        return whoMade;
    }

    public void setWhoMade(String whoMade) {
        this.whoMade = whoMade;
    }

    public String getIsSupply() {
        return isSupply;
    }

    public void setIsSupply(String isSupply) {
        this.isSupply = isSupply;
    }

    public String getWhenMade() {
        return whenMade;
    }

    public void setWhenMade(String whenMade) {
        this.whenMade = whenMade;
    }

    public Object getItemWeight() {
        return itemWeight;
    }

    public void setItemWeight(Object itemWeight) {
        this.itemWeight = itemWeight;
    }

    public Object getItemWeightUnits() {
        return itemWeightUnits;
    }

    public void setItemWeightUnits(Object itemWeightUnits) {
        this.itemWeightUnits = itemWeightUnits;
    }

    public Object getItemLength() {
        return itemLength;
    }

    public void setItemLength(Object itemLength) {
        this.itemLength = itemLength;
    }

    public Object getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(Object itemWidth) {
        this.itemWidth = itemWidth;
    }

    public Object getItemHeight() {
        return itemHeight;
    }

    public void setItemHeight(Object itemHeight) {
        this.itemHeight = itemHeight;
    }

    public String getItemDimensionsUnit() {
        return itemDimensionsUnit;
    }

    public void setItemDimensionsUnit(String itemDimensionsUnit) {
        this.itemDimensionsUnit = itemDimensionsUnit;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Object getRecipient() {
        return recipient;
    }

    public void setRecipient(Object recipient) {
        this.recipient = recipient;
    }

    public Object getOccasion() {
        return occasion;
    }

    public void setOccasion(Object occasion) {
        this.occasion = occasion;
    }

    public Object getStyle() {
        return style;
    }

    public void setStyle(Object style) {
        this.style = style;
    }

    public Boolean getNonTaxable() {
        return nonTaxable;
    }

    public void setNonTaxable(Boolean nonTaxable) {
        this.nonTaxable = nonTaxable;
    }

    public Boolean getIsCustomizable() {
        return isCustomizable;
    }

    public void setIsCustomizable(Boolean isCustomizable) {
        this.isCustomizable = isCustomizable;
    }

    public Boolean getIsDigital() {
        return isDigital;
    }

    public void setIsDigital(Boolean isDigital) {
        this.isDigital = isDigital;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }

    public Boolean getShouldAutoRenew() {
        return shouldAutoRenew;
    }

    public void setShouldAutoRenew(Boolean shouldAutoRenew) {
        this.shouldAutoRenew = shouldAutoRenew;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean getHasVariations() {
        return hasVariations;
    }

    public void setHasVariations(Boolean hasVariations) {
        this.hasVariations = hasVariations;
    }

    public Long getTaxonomyId() {
        return taxonomyId;
    }

    public void setTaxonomyId(Long taxonomyId) {
        this.taxonomyId = taxonomyId;
    }

    public List<String> getTaxonomyPath() {
        return taxonomyPath;
    }

    public void setTaxonomyPath(List<String> taxonomyPath) {
        this.taxonomyPath = taxonomyPath;
    }

    public Boolean getUsedManufacturer() {
        return usedManufacturer;
    }

    public void setUsedManufacturer(Boolean usedManufacturer) {
        this.usedManufacturer = usedManufacturer;
    }

}
