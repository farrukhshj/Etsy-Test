
package com.test.etsy.etsysearch.model.ListingImageEntity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Params {

    @SerializedName("listing_id")
    @Expose
    private String listingId;

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

}
