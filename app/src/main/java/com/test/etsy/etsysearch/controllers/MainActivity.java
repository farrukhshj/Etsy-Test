package com.test.etsy.etsysearch.controllers;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.test.etsy.etsysearch.R;
import com.test.etsy.etsysearch.model.ListingsEntity.ListingsEntity;
import com.test.etsy.etsysearch.network.DownloadService;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    public static final String DOWNLOAD_PROGRESS = "download_progress";
    public static final String TAG = "MainActivity";

    public RecyclerView rvListings;
    public LinearLayoutManager layoutManager;
    public ProgressDialog progressDialog;
    public EditText etKeyword;

    public int mCurrentOffset;
    public int mLisitingLimit;
    public String mkeyword;
    private boolean firstRun = true;
    private ListingsEntity mListingEntity;

    private HashMap<Long, String> mapOfImageUrls;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerBroadcastReceiver();

        mapOfImageUrls = new HashMap<>();

        mCurrentOffset = 0;
        mLisitingLimit = 20;

        rvListings = findViewById(R.id.rvListings);
        etKeyword = findViewById(R.id.etKeyword);

        layoutManager = new LinearLayoutManager(this);

        rvListings.setLayoutManager(layoutManager);
        rvListings.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            private int mPreviousTotal = 0;
            private boolean mLoading = true;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    int visibleItemCount = recyclerView.getChildCount();
                    int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    int firstVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                    if (mLoading) {
                        if (totalItemCount > mPreviousTotal) {
                            mLoading = false;
                            mPreviousTotal = totalItemCount;
                        }
                    }
                    int visibleThreshold = mLisitingLimit;
                    if (!mLoading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                        showProgressDialog();
                        getListingData(mCurrentOffset, mLisitingLimit, mkeyword);
                        mLoading = true;
                    }
                }
            }
        });

        etKeyword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (rvListings.getAdapter() != null) {
                        rvListings.getAdapter().getItemCount();
                        ListingsAdapter listingsAdapter = (ListingsAdapter) rvListings.getAdapter();
                        listingsAdapter.clearAdapter();
                    }
                    mkeyword = String.valueOf(etKeyword.getText());
                    hideKeyboard(v);
                    showProgressDialog();
                    getListingData(mCurrentOffset, mLisitingLimit, mkeyword);
                    return true;
                }
                return false;
            }
        });

    }

    /**
     * Helper function to hide the soft keyboard
     **/
    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Function that is called to fetch data from end-point using IntentService
     *
     * @param offset  is begining index of listings you want to download basically helpful in pagination
     * @param limit   is the number of listings you want to download ranging from 1 to 100. Default value is 25
     * @param keyword is the item you are trying to search
     **/
    private void getListingData(int offset, int limit, String keyword) {
        mCurrentOffset += mLisitingLimit;
        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra("OFFSET", offset);
        intent.putExtra("LIMIT", limit);
        intent.putExtra("KEYWORD", keyword);
        startService(intent);
    }


    /**
     * Helper function to register a local broadcast receiver to this activity
     **/
    private void registerBroadcastReceiver() {
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DOWNLOAD_PROGRESS);
        broadcastManager.registerReceiver(broadcastReceiver, intentFilter);
    }


    /**
     * Broadcast receiver that receives signals from various sources to know some task has been completed
     * like downloading a file or fetching data from end point etc
     **/
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Gson gson = new Gson();
            if (intent.getAction().equalsIgnoreCase(DOWNLOAD_PROGRESS)) {
                if (firstRun) {

                    mListingEntity = new ListingsEntity();
                    mListingEntity = gson.fromJson(intent.getStringExtra("LISTINGS_JSON"), ListingsEntity.class);
                    mapOfImageUrls = (HashMap<Long, String>) intent.getSerializableExtra("IMAGE_URLS");
                    Log.d(TAG, "Fetching data from endpoint successful");
                    bindDataToAdapter(mListingEntity, mapOfImageUrls);
                    firstRun = false;
                    hideProgressDialog();

                } else {

                    ListingsEntity temp;
                    temp = gson.fromJson(intent.getStringExtra("LISTINGS_JSON"), ListingsEntity.class);
                    mapOfImageUrls = (HashMap<Long, String>) intent.getSerializableExtra("IMAGE_URLS");
                    mListingEntity.getResults().addAll(temp.getResults());
                    rvListings.getAdapter().notifyDataSetChanged();
                    Log.d(TAG, "Fetching data from endpoint successful");
                    hideProgressDialog();

                }
            }
        }
    };


    /**
     * Function to bind the data fetched from web to the recycler view
     **/
    @SuppressLint("StaticFieldLeak")
    private void bindDataToAdapter(ListingsEntity mListingEntity, HashMap<Long, String> mapOfImageUrls) {
        if (mListingEntity != null && mListingEntity.getResults().size() != 0) {
            ListingsAdapter listingsAdapter = new ListingsAdapter(mListingEntity, mapOfImageUrls);
            rvListings.setAdapter(listingsAdapter);
        }
    }


    /**
     * Helper function to display progress dialog when doing some long operation
     **/
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading..");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /**
     * Helper function to hide progress dialog when completed doing some long operation
     **/
    public void hideProgressDialog() {
        progressDialog.dismiss();
    }
}
