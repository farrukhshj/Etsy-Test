package com.test.etsy.etsysearch.network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

public interface RetrofitInterface {
    @GET("listings/active")
    @Streaming
    Call<ResponseBody> getAllListings(@Query("limit") Integer limit, @Query("keywords") String keywords , @Query("offset") Integer offset ,@Query("api_key") String apiKey);

    @GET("listings/{listing_id}/images")
    @Streaming
    Call<ResponseBody> getImagesForListing(@Path("listing_id") Long listingId, @Query("api_key") String apiKey);
}
