
package com.test.etsy.etsysearch.model.ListingsEntity;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;

public class ListingsEntity {

    @SerializedName("count")
    private Long count;

    @SerializedName("results")
    private List<Result> results = null;

    @SerializedName("params")
    private Params params;

    @SerializedName("type")
    private String type;

    @SerializedName("pagination")
    private Pagination pagination;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public Params getParams() {
        return params;
    }

    public void setParams(Params params) {
        this.params = params;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

}
