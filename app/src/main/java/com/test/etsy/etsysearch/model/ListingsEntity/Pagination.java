
package com.test.etsy.etsysearch.model.ListingsEntity;

import com.google.gson.annotations.SerializedName;

public class Pagination {

    @SerializedName("effective_limit")
    private Long effectiveLimit;

    @SerializedName("effective_offset")
    private Long effectiveOffset;

    @SerializedName("next_offset")
    private Long nextOffset;

    @SerializedName("effective_page")
    private Long effectivePage;

    @SerializedName("next_page")
    private Long nextPage;

    public Long getEffectiveLimit() {
        return effectiveLimit;
    }

    public void setEffectiveLimit(Long effectiveLimit) {
        this.effectiveLimit = effectiveLimit;
    }

    public Long getEffectiveOffset() {
        return effectiveOffset;
    }

    public void setEffectiveOffset(Long effectiveOffset) {
        this.effectiveOffset = effectiveOffset;
    }

    public Long getNextOffset() {
        return nextOffset;
    }

    public void setNextOffset(Long nextOffset) {
        this.nextOffset = nextOffset;
    }

    public Long getEffectivePage() {
        return effectivePage;
    }

    public void setEffectivePage(Long effectivePage) {
        this.effectivePage = effectivePage;
    }

    public Long getNextPage() {
        return nextPage;
    }

    public void setNextPage(Long nextPage) {
        this.nextPage = nextPage;
    }

}
